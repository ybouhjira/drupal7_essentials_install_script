#!/bin/bash

drush dl admin_menu
drush en admin_menu_toolbar -y
drush dis toolbar -y

drush dl module_filter
drush en module_filter -y

drush dl views
drush en views_ui -y

drush dl date
drush en date_popup -y

drush dl features
drush en features -y

drush dl autocomplete_deluxe
drush en autocomplete_deluxe -y

# Drupal Commerce admin theme
drush dl shiny
drush vset admin_theme shiny

drush dl devel
drush en devel -y